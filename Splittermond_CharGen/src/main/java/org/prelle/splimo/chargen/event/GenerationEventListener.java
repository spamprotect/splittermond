/**
 * 
 */
package org.prelle.splimo.chargen.event;

/**
 * @author prelle
 *
 */
public interface GenerationEventListener {

	//-------------------------------------------------------------------
	public void handleGenerationEvent(GenerationEvent event);
	
}
