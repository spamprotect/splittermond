/**
 * 
 */
package org.prelle.splimo.charctrl;


/**
 * @author prelle
 *
 */
public interface GeneratingResourceController extends ResourceController, Generator {

	//-------------------------------------------------------------------
	public void setAllowMaxResources(boolean allow);

	//-------------------------------------------------------------------
	public boolean isAllowMaxResources();
	
}
