/**
 * 
 */
package org.prelle.splimo.modifications;

/**
 * @author prelle
 *
 */
public enum ModificationSource {

	EQUIPMENT,
	MAGICAL,
	OTHER
	
}
